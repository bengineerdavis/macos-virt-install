# add lines to convenience script
# https://www.makeuseof.com/macos-ubuntu-linux-virtual-machine/
# repo: https://github.com/foxlet/macOS-Simple-KVM

cat <<EOT >> ~/KVM/macOS/basic.sh

-drive id=SystemDisk,if=none,file=mac_os.qcow2

-device ide-hd,bus=sata.4,drive=SystemDisk

EOT